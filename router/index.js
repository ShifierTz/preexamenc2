import express from "express";

export const router = express.Router();
export default { router };

router.get("/", (req, res) => {
	const params = {
		nombre: req.query.nombre,
		domicilio: req.query.domicilio,
		servicio: req.query.servicio,
		kW: req.query.kW
	};
	res.render("pago", params);
});

router.post("/", (req, res) => {
	const params = {
		nombre: req.body.nombre,
		domicilio: req.body.domicilio,
		servicio: req.body.servicio,
		kW: req.body.kW
	};
	res.render("tabla", params);
});